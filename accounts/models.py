from django.db import models
from django.contrib.auth.models import AbstractUser

from photos.models import AlbumToken, Photo, Album


class CustomUser(AbstractUser):
	# class created for eventuality of needing additional fields for user
	pass

	def __str__(self):
		return self.username


class ContactMessage(models.Model):
	name = models.CharField(max_length=255)
	email = models.EmailField()
	message = models.TextField()
	date_sent = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.email

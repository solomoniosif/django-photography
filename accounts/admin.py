from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser, ContactMessage


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
	add_form = CustomUserCreationForm
	form = CustomUserChangeForm
	model = CustomUser
	list_display = ['email', 'username', ]


@admin.register(ContactMessage)
class ContactMessageAdmin(admin.ModelAdmin):
	fields = [('name', 'email', 'date_sent'), 'message']
	readonly_fields = ['date_sent', 'message']
	list_display = ('name', 'email', 'date_sent', 'truncated_message')
	search_fields = ['name', 'email']

	@admin.display(description='Message')
	def truncated_message(self, obj):
		message = obj.message
		if len(message) < 75:
			return message
		else:
			return message[:72] + '...'

	class Meta:
		model = ContactMessage

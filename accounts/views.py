from _ast import Delete

from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib.auth.views import LoginView
from django.contrib.auth import login
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, FormView, DeleteView

from .forms import CustomUserCreationForm, ContactMessageForm
from .models import ContactMessage


class UserLoginView(LoginView):
	template_name = 'registration/login.html'
	fields = '__all__'
	redirect_authenticated_user = True

	def get_success_url(self):
		return reverse_lazy('photos:home')


class SignUpView(CreateView):
	template_name = 'registration/signup.html'
	form_class = CustomUserCreationForm
	success_url = reverse_lazy('photos:home')

	def form_valid(self, form):
		user = form.save()
		if user is not None:
			login(self.request, user)
		return super(SignUpView, self).form_valid(form)


def send_message(request):
	if request.is_ajax():
		name = request.POST.get('name', None)
		# email = request.POST.get('email', None)
		# message = request.POST.get('message', None)
		contact_form = ContactMessageForm(request.POST)
		if contact_form.is_valid():
			contact_form.save()
			return JsonResponse({'msg': f'Thank you {name}! Your message was successfully sent!'})

		return JsonResponse({'msg': 'There was an error sending your message.'})


class ContactFormView(FormView):
	template_name = 'photos/index.html'
	form_class = ContactMessageForm
	success_url = 'photos:home'

	def form_valid(self, form):
		return super().form_valid(form)


class ContactMessageListView(ListView):
	model = ContactMessage
	template_name = 'accounts/message_list.html'
	context_object_name = 'messages'
	ordering = ['-date_sent']

	def get_queryset(self):
		qs = super().get_queryset()
		if self.request.user.is_staff:
			return qs
		return None


class ContactMessageDetailView(DetailView):
	model = ContactMessage
	template_name = 'accounts/message_detail.html'
	context_object_name = 'message'


class ContactMessageDeleteView(DeleteView):
	model = ContactMessage
	success_url = reverse_lazy('accounts:messages')
	template_name = 'accounts/contact_message_confirm_delete.html'
	context_object_name = 'message'

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.text import slugify

from .models import Post
from photos.models import Photo, Album


@receiver(post_save, sender=Post)
def post_album_to_photos_post_save(sender, instance, *args, **kwargs):
	if instance.album and not instance.pk:
		current_post = Post.objects.get(id=instance.id)
		post_album = current_post.album
		photos = post_album.prefetch_related('photos')
		for photo in photos:
			current_post.photos.add(photo)
		current_post.save()

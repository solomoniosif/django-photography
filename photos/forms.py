from django import forms

from .models import Photo, Album, AlbumToken


class PhotoForm(forms.ModelForm):
	class Meta:
		model = Photo
		fields = '__all__'


PhotoFormSet = forms.modelformset_factory(
	Photo, fields=('title', 'album', 'tags', 'image', 'is_featured'), extra=1)


class AlbumForm(forms.ModelForm):
	class Meta:
		model = Album
		fields = ['title', 'status', 'is_featured']


class AlbumTokenForm(forms.ModelForm):
	class Meta:
		model = AlbumToken
		fields = '__all__'
		widgets = {
			'album': forms.widgets.HiddenInput(),
			'token': forms.widgets.URLInput(attrs={'readonly': True}),
			'expiry_date': forms.widgets.DateInput(attrs={'type': 'date'})
		}

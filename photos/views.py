from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.utils import timezone, dateformat
from django.views.generic import ListView, TemplateView, CreateView, DeleteView
from django.contrib import messages
from extra_views import InlineFormSetFactory, CreateWithInlinesView, UpdateWithInlinesView, SuccessMessageMixin
from hitcount.views import HitCountDetailView

from accounts.forms import ContactMessageForm
from blog.models import Post
from .forms import PhotoForm, PhotoFormSet, AlbumForm, AlbumTokenForm
from .models import Photo, Album, AlbumToken, PrivatePhoto


def home(request):
	return render(request, 'photos/index.html')


class HomePageView(TemplateView):
	template_name = 'photos/index.html'

	def get_context_data(self, **kwargs):
		context = super(HomePageView, self).get_context_data(**kwargs)
		latest_albums = Album.objects.published()[:7]
		latest_posts = Post.objects.published()[:7]
		context['albums'] = latest_albums
		context['posts'] = latest_posts
		return context


class PhotoDetailView(DeleteView):
	model = Photo
	template_name = 'photos/photo_detail_view.html'
	context_object_name = 'photo'


class PhotoListView(ListView):
	model = Photo
	template_name = 'photos/photo_list.html'
	context_object_name = 'photos'


class PhotoCreateView(TemplateView):
	template_name = 'photos/add_photos.html'

	def get(self, *args, **kwargs):
		formset = PhotoFormSet(queryset=Photo.objects.none())
		return self.render_to_response({'photo_formset': formset})

	def post(self, *args, **kwargs):
		formset = PhotoFormSet(data=self.request.POST)
		if formset.is_valid():
			formset.save()
			return redirect(reverse_lazy('photo_list'))
		return self.render_to_response({'photo_formset': formset})


def upload_photo(request):
	context = dict(backend_form=PhotoForm())
	if request.method == 'POST':
		form = PhotoForm(request.POST, request.FILES)
		context['posted'] = form.instance
		if form.is_valid():
			form.save()
	return render(request, 'photos/add_photo.html', context)


class AlbumCreateView(LoginRequiredMixin, CreateView):
	model = Album
	template_name = 'photos/album_create_bulk.html'
	form_class = AlbumForm
	success_url = None

	def form_valid(self, form):
		form.instance.author = self.request.user
		self.object = form.save()
		photos = self.request.FILES.getlist('photos')
		if photos:
			for photo in photos:
				Photo.objects.create(
					album=self.object,
					image=photo
				)
		messages.success(self.request, f"<b>{self.object.title}</b> created successfully")
		return HttpResponseRedirect(self.get_success_url())

	def get_success_url(self):
		return reverse_lazy('photos:album_detail', kwargs={'slug': self.object.slug})


class AlbumPhotoInlineFormset(InlineFormSetFactory):
	model = Photo
	form_class = PhotoForm
	fields = ['image', 'is_featured']
	extra_forms = 1
	can_delete = True

	def get_factory_kwargs(self):
		factory_kwargs = super().get_factory_kwargs()
		factory_kwargs.update({'extra': self.extra_forms})
		return factory_kwargs


class AlbumCreateWithInlinesView(LoginRequiredMixin, SuccessMessageMixin, CreateWithInlinesView):
	model = Album
	form_class = AlbumForm
	inlines = [AlbumPhotoInlineFormset, ]
	template_name = 'photos/album_create.html'
	success_url = None
	success_message = "Album <b>%(title)s</b> was created successfully"

	def get_success_url(self):
		return reverse_lazy('photos:album_detail', kwargs={'slug': self.object.slug})


class AlbumUpdateWithInlinesView(LoginRequiredMixin, SuccessMessageMixin, UpdateWithInlinesView):
	model = Album
	form_class = AlbumForm
	inlines = [AlbumPhotoInlineFormset, ]
	template_name = 'photos/album_update.html'
	success_message = "Album <b>%(title)s</b> was updated successfully"


class AlbumListView(ListView):
	model = Album
	template_name = 'photos/album_list.html'
	context_object_name = 'albums'

	def get_queryset(self):
		qs = super().get_queryset()
		if self.request.user.is_staff:
			return qs
		return qs.filter(status='PU')


class AlbumDetailView(HitCountDetailView):
	model = Album
	template_name = 'photos/album_detail.html'
	context_object_name = 'album'
	count_hit = True

	def get_object(self, queryset=None):
		slug = self.kwargs.get(self.slug_url_kwarg)
		album = get_object_or_404(Album, slug=slug)
		if self.request.user.is_staff:
			return album
		if album.status == 'PR':
			messages.warning(self.request, "This album is private")
			return Album.objects.none()
		elif album.status == 'DR':
			messages.warning(self.request, "This album is not yet published")
			return Album.objects.none()
		else:
			return album

	def get_context_data(self, **kwargs):
		context = super(AlbumDetailView, self).get_context_data(**kwargs)
		if self.request.user.is_staff and self.object.is_private:
			context['album_token_form'] = AlbumTokenForm(initial={'album': self.get_object()})
			album_tokens = self.object.album_token.all() or None
			context['album_tokens'] = album_tokens
		return context


class PrivateAlbumListView(ListView):
	model = Album
	template_name = 'photos/private_album_list.html'
	context_object_name = 'albums'

	def get_queryset(self):
		qs = super().get_queryset()
		return qs.filter(status='PR')


class PrivateAlbumView(HitCountDetailView):
	model = Album
	template_name = 'photos/private_album.html'
	context_object_name = 'album'
	count_hit = True

	def get_object(self, queryset=None):
		token = self.kwargs.get('token', None)
		album_token = get_object_or_404(AlbumToken, token=token)
		album = album_token.album

		if album_token.expiry_date > timezone.now():
			return album
		else:
			messages.error(self.request,
						   f"Your access to the private album <b>{album.title}</b> expired on {dateformat.format(album_token.expiry_date, 'd F Y')}.")
			return Album.objects.none()

	def get_context_data(self, **kwargs):
		context = {}
		token = self.kwargs.get('token', None)
		if token:
			album_token = get_object_or_404(AlbumToken, token=token)
			context['album_token_id'] = album_token.id
			album = album_token.album
			favorite_photos_ids = []
			for photo in album.photos.all():
				private_photo = PrivatePhoto.objects.get(album_token=album_token, photo=photo)
				if private_photo.is_favorite:
					favorite_photos_ids.append(photo.id)
			context['favorite_photos'] = favorite_photos_ids
		context.update(kwargs)
		return super().get_context_data(**context)


class AlbumDeleteView(DeleteView):
	model = Album
	success_url = reverse_lazy('photos:albums')


class SearchView(TemplateView):
	template_name = 'photos/search_results.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		query = self.request.GET.get('q')
		posts = Post.objects.distinct().filter(
			Q(title__icontains=query) | Q(text__icontains=query) | Q(tags__name__icontains=query))
		albums = Album.objects.distinct().filter(Q(title__icontains=query))
		if not posts.exists() and not albums.exists():
			messages.error(self.request, f"No search results for <b>{query}</b>")
		else:
			context['posts'] = posts
			context['albums'] = albums
			messages.success(self.request, f"Search results for <b>{query}</b>")
		return context


def create_token(request, slug):
	album = get_object_or_404(Album, slug=slug)  # Album.objects.get(slug=slug)
	if request.method == 'POST':
		form = AlbumTokenForm(request.POST)
		if form.is_valid():
			form.save(commit=False)
			form.save()
			messages.success(request, "New token created successfully")
			return HttpResponseRedirect(reverse('photos:album_detail', args=(album.slug,)))
	return HttpResponseRedirect(reverse('photos:album_detail', args=(album.slug,)))


def update_token(request, pk):
	token = AlbumToken.objects.get(id=pk)
	album = token.album
	form = AlbumTokenForm(request.POST or None, instance=token)

	if form.is_valid():
		form.save()
		messages.success(request, f"Token updated successfully")
		return redirect(reverse_lazy('photos:album_detail', kwargs={'slug': album.slug}))
	return redirect(reverse_lazy('photos:album_detail', kwargs={'slug': album.slug}))


def delete_token(request, pk):
	token = AlbumToken.objects.get(id=pk)
	album = token.album
	token.delete()
	messages.success(request, "Token deleted successfully")
	return redirect(reverse_lazy('photos:album_detail', kwargs={'slug': album.slug}))


def like_photo(request):
	if request.POST.get('action') == 'post':
		photo_id = int(request.POST.get('photo_id'))
		photo = get_object_or_404(Photo, id=photo_id)
		album_token_id = int(request.POST.get('album_token_id'))
		album_token = get_object_or_404(AlbumToken, id=album_token_id)
		private_photo = get_object_or_404(PrivatePhoto, album_token=album_token, photo=photo)

		if private_photo.is_favorite:
			private_photo.is_favorite = False
			private_photo.save()
		else:
			private_photo.is_favorite = True
			private_photo.save()
		return JsonResponse({'is_liked': private_photo.is_favorite, 'photo_id': photo_id, })

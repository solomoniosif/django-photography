from django.contrib import admin

from photos.models import Album, AlbumToken, Photo, PrivatePhoto


class PhotoInline(admin.TabularInline):
	model = Photo


@admin.register(Album)
class AlbumAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug': ('title',)}
	list_display = ('title', 'photos', 'is_featured')
	list_editable = ('is_featured',)
	inlines = [PhotoInline]

	def get_queryset(self, request):
		return super().get_queryset(request).prefetch_related('photos')

	@admin.display(description="Photos")
	def photos(self, obj):
		return obj.photos.all().count()


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug': ('title',)}
	list_display = ('title', 'tag_list', 'album', 'is_featured')
	list_editable = ('is_featured',)

	# Get list of tags
	def get_queryset(self, request):
		return super().get_queryset(request).prefetch_related('tags')

	@admin.display(description="Tags")
	def tag_list(self, obj):
		return u", ".join(o.name for o in obj.tags.all())


@admin.register(AlbumToken)
class AlbumTokenAdmin(admin.ModelAdmin):
	list_display = ['album', 'token', 'creation_date', 'expiry_date']


@admin.action(description='Mark selected photos as favorite')
def make_favorite(modeladmin, request, queryset):
	queryset.update(is_favorite=True)


@admin.register(PrivatePhoto)
class FavoritePhotoAdmin(admin.ModelAdmin):
	list_display = ('photo', 'album_token', 'is_favorite')
	list_filter = ('album_token', 'is_favorite')
	actions = [make_favorite]

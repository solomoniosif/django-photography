import secrets

from django.contrib.sites.models import Site
from django.db import models
from django.utils import timezone
from django.db.models.signals import pre_save, post_save
from django.urls import reverse
from taggit.managers import TaggableManager
from cloudinary.models import CloudinaryField

from core.models import BasePublishModel
from blog.models import Post
from core.utils import slugify_pre_save


class Album(BasePublishModel):
	title = models.CharField(max_length=200, null=False, blank=False)
	slug = models.SlugField(null=True, blank=True, db_index=True)
	is_featured = models.BooleanField(default=False)

	def __str__(self):
		return self.title

	def get_featured_image_url(self):
		images = self.photos.all()
		if images.exists():
			try:
				featured_image = images.filter(is_featured=True).first()
				return featured_image.image.url
			except:
				featured_image = images.first()
				return featured_image.image.url
		else:
			return ''

	def get_absolute_url(self):
		return reverse('photos:album_detail', args=[self.slug])

	class Meta:
		verbose_name = 'Album'
		verbose_name_plural = 'Albums'
		ordering = ['-is_featured', '-id']


pre_save.connect(slugify_pre_save, sender=Album)


def generate_token():
	return secrets.token_urlsafe(16)[:16]


def get_default_token_expiry_date(weeks=26):
	return timezone.localtime(timezone.now()) + timezone.timedelta(weeks=weeks)


class Photo(models.Model):
	image = CloudinaryField("Image", overwrite=True, resource_type="image", transformation={"quality": "auto:eco"})
	description = models.TextField(null=True, blank=True)
	album = models.ForeignKey(Album, on_delete=models.SET_NULL, null=True, blank=True, related_name="photos")
	post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name="post_photos", null=True, blank=True)
	title = models.CharField(max_length=250, null=True, blank=True)
	tags = TaggableManager(blank=True, help_text=None)
	alt = models.CharField(max_length=60, default="error.jpg")
	slug = models.SlugField(null=True, blank=True, unique=True, db_index=True)
	is_featured = models.BooleanField(default=False)

	def __str__(self):
		return self.title

	def save(self, *args, **kwargs):
		if not self.title:
			self.title = self.image.name
		super(Photo, self).save(*args, **kwargs)

	def get_absolute_url(self):
		return reverse('photos:photo_detail', args=[self.id])

	class Meta:
		verbose_name = "photo"
		verbose_name_plural = "photos"
		ordering = ['-id']


pre_save.connect(slugify_pre_save, sender=Photo)


class AlbumToken(models.Model):
	album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='album_token')
	token = models.CharField(max_length=255, default=generate_token)
	creation_date = models.DateTimeField(auto_now_add=True)
	expiry_date = models.DateTimeField(default=get_default_token_expiry_date)

	def __str__(self):
		return f"Token #{self.id} for '{self.album.title}'"

	def is_expired(self):
		return self.expiry_date > timezone.now()

	def get_absolute_url(self):
		return reverse('photos:private_album', args=[self.token])

	def get_full_url(self):
		domain = Site.objects.get_current().domain
		path = self.get_absolute_url()
		url = f'http://{domain}{path}'
		return url

	class Meta:
		verbose_name = 'Album Token'
		verbose_name_plural = 'Albums Tokens'


def create_private_photos(sender, instance, *args, **kwargs):
	album = instance.album
	for photo in album.photos.all():
		PrivatePhoto.objects.create(
			album_token=instance,
			photo=photo
		)


post_save.connect(create_private_photos, sender=AlbumToken)


class PrivatePhoto(models.Model):
	album_token = models.ForeignKey(AlbumToken, on_delete=models.CASCADE, related_name='album_token')
	photo = models.ForeignKey(Photo, on_delete=models.CASCADE, related_name='private_photo')
	is_favorite = models.BooleanField(default=False)

	def __str__(self):
		return f""

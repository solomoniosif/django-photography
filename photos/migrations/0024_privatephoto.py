# Generated by Django 3.2.6 on 2021-09-11 11:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0023_remove_albumtoken_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='PrivatePhoto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_favorite', models.BooleanField(default=False)),
                ('album_token', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='album_token', to='photos.albumtoken')),
                ('photo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='private_photo', to='photos.photo')),
            ],
        ),
    ]

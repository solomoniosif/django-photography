# Generated by Django 3.2.6 on 2021-09-01 05:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0016_alter_albumtoken_token'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='photo',
            options={'ordering': ['-id'], 'verbose_name': 'photo', 'verbose_name_plural': 'photos'},
        ),
        migrations.AlterField(
            model_name='albumtoken',
            name='token',
            field=models.CharField(default='xZ_OrVlJmlqCg6_VMeTGqg', max_length=255),
        ),
    ]

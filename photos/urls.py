from django.urls import path
from . import views

app_name = 'photos'

urlpatterns = [
	path('', views.HomePageView.as_view(), name='home'),
	path('photos/<int:pk>/', views.PhotoDetailView.as_view(), name='photo_detail'),
	path('photos/', views.PhotoListView.as_view(), name='photo_list'),
	path('photos/add/', views.PhotoCreateView.as_view(), name='add_photos'),
	path('photos/add-photo/', views.upload_photo, name='add_photo'),

	path('albums/', views.AlbumListView.as_view(), name='albums'),
	path('albums/<slug:slug>', views.AlbumDetailView.as_view(), name='album_detail'),
	path('albums/create/', views.AlbumCreateWithInlinesView.as_view(), name='album_create'),
	path('albums/create-bulk/', views.AlbumCreateView.as_view(), name='album_create_bulk'),
	path('albums/update/<slug:slug>', views.AlbumUpdateWithInlinesView.as_view(), name='album_update'),
	path('albums/delete/<slug:slug>', views.AlbumDeleteView.as_view(), name='album_delete'),

	path('search/', views.SearchView.as_view(), name='search'),

	path('private/<str:token>/', views.PrivateAlbumView.as_view(), name='private_album'),
	path('private-albums/', views.PrivateAlbumListView.as_view(), name='private_albums'),
	path('create-token/<slug:slug>/', views.create_token, name='create_token'),
	path('update-token/<int:pk>/', views.update_token, name='update_token'),
	path('delete-token/<int:pk>/', views.delete_token, name='delete_token'),

	path('like-photo/', views.like_photo, name='like_photo'),

]
